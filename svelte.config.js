import preprocess from "svelte-preprocess";
import faviconsPlugin from '@darkobits/vite-plugin-favicons';

//import adapter from "./plugins/adapter.js";
import adapter from '@sveltejs/adapter-cloudflare-workers';


/** @type {import('@sveltejs/kit').Config} */
const config = {
  kit: {
    // hydrate the <div id="svelte"> element in src/app.html
    target: '#svelte',
    adapter: adapter(),
    vite: {
      plugins: [
        faviconsPlugin({
          icons: {
            favicons: {
              source: './static/favicon.png'
            },
          }
        })
      ]
    },
  },

  preprocess: [
    preprocess({
      "postcss": true
    }),
  ]
};

export default config;