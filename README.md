### This repository has been archived

The new version of the website uses static-site generation instead of server-side rendering like this version.

# A digital newspaper website

## How it works

The process from writing words to showing up on a website is split into 4 steps, each with a repository.

First, text content (like articles), is written in Markdown files in the `newspaper` repository. Second, images and files that relate to an article or comic are put on the storage server (currently a FileGator instance). Third, a script in the `meta` repository downloads the text content and converts it to JSON. It also turns the Markdown into HTML. The script additionally finds the linked images and creates URLs to them. The script outputs the JSON files to GitLab Pages. Fourthly, and last, the code in this repository is the (SvelteKit) website. It requests data from the `meta` server.

## Developing

Once you've created a project and installed dependencies with `pnpm install`, start a development server:

```bash
pnpm dev
```

## Building

This SvelteKit app is deployed to a Cloudflare Worker. 

```bash
pnpm build
```

> You can preview the built app with `pnpm preview`, regardless of whether you installed an adapter. This should _not_ be used to serve your app in production.

## Publish

You need to update the `wrangler.toml` file to point to your Cloudflare Worker, and login with `wrangler`.

```bash
wrangler publish
```
